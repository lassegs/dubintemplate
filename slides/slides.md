# Hello World

This is the first slide

---

Horizontal slides with `---`

Vertical slides with `--`

--

## Hello!

---

![image](https://anti-brands.fra1.digitaloceanspaces.com/brands/forskningsradet/media/_1200x676_fit_center-center_82_none/Forskningsdagene_Brandbook_Vertk%C3%B8ykasse_04_BrukAvMonster.jpg)

Test

---

<!-- .slide: data-background="https://anti-brands.fra1.digitaloceanspaces.com/brands/forskningsradet/media/_1200x800_fit_center-center_82_none/Forskningsradet_Temabilde_Arrangementer_TeknologiDigitalisering_01_@1x_RGB.jpg" data-background-opacity="0.33"-->

## Nice, a background!

---

<!-- .slide: data-autoslide="2000" data-background-video="https://anti-brands.fra1.digitaloceanspaces.com/brands/forskningsradet/media/NFR_STINGER_PICTURE_16-9.mp4" -->

---


🫢 HTML in .md

<div class="halfsplit">
<div class="gridleft"> 

### Half
Grid left

</div>
<div class="gridright">

### Split
Grid right

</div>
</div>


Note: 
Available by pressing '**S**' in the presentation. Notes are separated like - newline and a line saying 'Note:'

---

## [Fork away](https://gitlab.com/lassegs/dubintemplate/)

---

<!-- .slide:  data-background-video="https://anti-brands.fra1.digitaloceanspaces.com/brands/forskningsradet/media/NFR_ENDPOSTER-16-9_LIME_SHAPE.mp4" data-background-opacity="1"-->